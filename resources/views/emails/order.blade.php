<div style="margin-bottom: 15px;">
    <b>Name:</b> {{ $orderName }}
</div>
<div style="margin-bottom: 15px;">
    <b>Astrologer:</b> {{ $orderAstrologer }}
</div>
<div style="margin-bottom: 15px;">
    <b>Service:</b> {{ $orderService }}
</div>
<div>
    <b>Price:</b> {{ $orderPrice }}
</div>
