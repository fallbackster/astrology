<?php


namespace App\Requests;

use \Illuminate\Http\Request;


class OrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'name' => 'required|string',
            'user_id' => 'required|numeric',
            'service_id' => 'required|numeric',
        ];
    }

    public function all($keys = null)
    {
        return $this->json()->all();
    }
}
