<?php

namespace App\Services;

use App\Services\Interfaces\HttpRequestInterface;
use GuzzleHttp\Client;

class DummyHttpRequestClient  implements HttpRequestInterface
{
    public function request($method,$uri='',$params = []){
        return $this->getOrderPaymentResponse();
    }

    private function getOrderPaymentResponse(){
        return 'authorized';
    }
}
