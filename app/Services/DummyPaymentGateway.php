<?php

namespace App\Services;

use App\Models\Order;
use App\Services\Interfaces\HttpRequestInterface;
use App\Services\Interfaces\PaymentGatewayInterface;


class DummyPaymentGateway  implements PaymentGatewayInterface
{
    protected $order;
    protected $client;

    public function __construct(){
        $this->client = new DummyHttpRequestClient();
    }


    /**
     * @param Order $order
     */
    public function setOrder(Order $order){
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getPaymentResult(){
        return $this->client->request('GET','/',['provider'=>'PayPal']);
    }
}
