<?php
namespace App\Services\Interfaces;


use App\Models\Order;

interface PaymentGatewayInterface
{
    const PAYMENT_RESULT_OK = 'authorized';
    const PAYMENT_RESULT_DECLINED = 'declined';
    const PAYMENT_RESULT_CANCELLED_BY_CARDHOLDER = 'cancelledByCardHolder';
    const PAYMENT_RESULT_FAILED = 'failed';
    const PAYMENT_TIMED_OUT = 'timeout';

    /**
     * @param $order
     */
    public function setOrder(Order $order);

    /**
     * @return string
     */
    public function getPaymentResult();

}
