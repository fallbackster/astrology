<?php
namespace App\Services\Interfaces;

use App\Models\Order;

interface MessageInterface
{
    public function sendMessage();

    public function setOrder(Order $order);
}
