<?php
namespace App\Repositories\Interfaces;

interface ServiceUserRepositoryInterface
{
    public function getPriceByServiceUser(int $userId, int $serviceId);
}
