<?php

namespace App\Repositories;

use App\Repositories\Interfaces\OrderRepositoryInterface;
use App\Models\Order;


class OrderRepository implements OrderRepositoryInterface
{
    /**
     * @param array $array
     * @return Order
     */
    public function store(array $array): Order
    {
        $order = new Order;
        $order->setRawAttributes($array);
        $order->created_at = date('Y-m-d H:i:s');
        $order->updated_at = date('Y-m-d H:i:s');
        $order->save();
        return $order;
    }
}
