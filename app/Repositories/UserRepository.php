<?php

namespace App\Repositories;

use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class UserRepository implements UserRepositoryInterface
{
    protected $itemsPerPage;

    /**
     * UserRepository constructor.
     * @param $itemsPerPage
     */
    public function __construct($itemsPerPage){
        $this->itemsPerPage = $itemsPerPage;
    }

    /**
     * @return Collection
     */
    public function all()
    {
        return User::with('services:name')->paginate($this->itemsPerPage);
    }

    /**
     * @param int $id
     * @return Collection
     */
    public function getOne(int $id)
    {
        return User::with('services:id,name,service_user.price')->where('id', $id)->firstOrFail();
    }

}
