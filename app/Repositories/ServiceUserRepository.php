<?php

namespace App\Repositories;

use App\Models\ServiceUser;
use App\Repositories\Interfaces\ServiceUserRepositoryInterface;

class ServiceUserRepository implements ServiceUserRepositoryInterface
{
    /**
     * @param int $userId
     * @param int $serviceId
     * @return int
     * @throws \Exception
     */
    public function getPriceByServiceUser(int $userId,int $serviceId)
    {
        $record = ServiceUser::where('service_id',$serviceId)->where('user_id',$userId)->firstOrFail();
        return $record->price;
    }
}
