<?php

namespace App\Mail;

use App\Models\Order;
use App\Services\Interfaces\MessageInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class OrderShipped extends Mailable implements MessageInterface
{
    use Queueable, SerializesModels;

    protected $order;

    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("Your order number #{$this->order->id} placed succesful")
            ->view('emails.order')
            ->with([
                'orderName' => $this->order->name,
                'orderAstrologer' => $this->order->user->name,
                'orderService'=>$this->order->service->name,
                'orderPrice' => $this->order->price,
            ]);
    }

    public function sendMessage()
    {
        Mail::to([$this->order->email])->send($this);
    }
}
