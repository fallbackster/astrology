<?php

namespace App\Jobs;

use App\Models\Order;
use App\Services\GuzzleHttpRequestClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class ProcessOrderToSpreedSheet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const REQUEST_URL = 'https://script.google.com/macros/s/AKfycbxAr3wba5Bp2CY2pZUxx6xXsSFf6A-CAtJQZquaTgj5YeuTWkYo65O3yQ/exec';

    protected $order;
    protected $client;

    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return GuzzleHttpRequestClient
     */
    protected function getClient()
    {
        return new GuzzleHttpRequestClient(['base_uri' => self::REQUEST_URL]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = [
            'id' => $this->order->id,
            'name' => $this->order->name,
            'email' => $this->order->email,
            'price' => $this->order->price,
            'service' => $this->order->service->name,
            'astrologer' => $this->order->user->name
        ];

        $client = $this->getClient();
        $query = http_build_query($order);

        Redis::throttle('throttle:queues:processjob')->allow(1)->every(5)->then(function () use ($order,$client,$query) {
            $client->request('GET', '', [
                'query' => $query
            ]);
        });
    }
}
