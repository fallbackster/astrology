<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param \Exception $e
     * @param array|string|null $message
     * @return array
     */
    protected function prepareErrorAnswer(\Exception $e, $message = null): array
    {
        $error = ['error' => []];
        if ($message) {
            if (is_array($message)) {
                $messageString = '';
                foreach ($message as $attribute => $err) {
                    $messageString .= current($err);
                }
                $error['error']['message'] = $message ?? $e->getMessage();
            } else {
                $error['error']['message'] = $message;
            }
        } else {
            $error['error']['message'] = $e->getMessage();
        }
        $error['error']['code'] = $e->getCode();
        $error['error']['type'] = get_class($e);
        return $error;
    }




}
