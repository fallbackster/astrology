<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessOrderToSpreedSheet;
use App\Repositories\Interfaces\OrderRepositoryInterface;
use App\Repositories\Interfaces\ServiceUserRepositoryInterface;
use App\Requests\OrderRequest;
use App\Services\Interfaces\MessageInterface;
use App\Services\Interfaces\PaymentGatewayInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;


class OrderController extends Controller
{
    private $orderRepository;
    private $serviceUserRepository;
    private $message;
    private $spreadSheetService;
    private $paymentGateway;


    /**
     * OrderController constructor.
     * @param OrderRepositoryInterface $orderRepository
     * @param ServiceUserRepositoryInterface $serviceUserRepository
     * @param MessageInterface $message
     * @param ProcessOrderToSpreedSheet $spreadSheetService
     * @param PaymentGatewayInterface $paymentGateway
     */
    public function __construct(OrderRepositoryInterface $orderRepository,
                                ServiceUserRepositoryInterface $serviceUserRepository,
                                MessageInterface $message,
                                ProcessOrderToSpreedSheet $spreadSheetService,
                                PaymentGatewayInterface $paymentGateway
    )
    {
        $this->orderRepository = $orderRepository;
        $this->serviceUserRepository = $serviceUserRepository;
        $this->message = $message;
        $this->spreadSheetService = $spreadSheetService;
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * @param OrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function create(OrderRequest $request)
    {
        try {
            $request->validate($request->rules());
            $order = $this->orderRepository->store(array_merge($request->all(), ['price' => $this->serviceUserRepository->getPriceByServiceUser($request->user_id, $request->service_id)]));
            /** functionality for future using real payment service */
            $this->paymentGateway->setOrder($order);
            if($this->paymentGateway->getPaymentResult()){
                $this->message->setOrder($order);
                $this->message->sendMessage();
                $this->spreadSheetService->setOrder($order);
                $this->dispatch($this->spreadSheetService);
                return response()->json($order, 200);
            }else{
                return response()->json($this->prepareErrorAnswer(new \Exception('Payment declined')), 402);
            }
        } catch (ValidationException $e) {
            return response()->json($this->prepareErrorAnswer($e, $e->errors()), 400);
        } catch (ModelNotFoundException $e) {
            return response()->json($this->prepareErrorAnswer($e, 'Hacking attempt!!!'), 400);
        } catch (\Exception $e) {
            return response()->json($this->prepareErrorAnswer($e), 400);
        }
    }
}
