<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\UserRepositoryInterface;

class UserController extends Controller
{
    /** @var $userRepository UserRepositoryInterface */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->userRepository->all();
    }

    /**
     * @param $id int
     * @return mixed
     */
    public function show(int $id)
    {
        try {
            return $this->userRepository->getOne($id);
        } catch (\Exception $e) {
            return response()->json($this->prepareErrorAnswer($e), 404);
        }
    }


}
