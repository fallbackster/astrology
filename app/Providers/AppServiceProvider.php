<?php
namespace App\Providers;

use App\Mail\OrderShipped;
use App\Services\DummyPaymentGateway;
use App\Services\Interfaces\MessageInterface;
use App\Services\Interfaces\PaymentGatewayInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            MessageInterface::class,
            OrderShipped::class
        );

        $this->app->bind(
            PaymentGatewayInterface::class,
            DummyPaymentGateway::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
