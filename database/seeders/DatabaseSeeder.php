<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use \App\Models\User;
use \App\Models\Service;
use \App\Models\ServiceUser;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $userIds = User::factory(15)->create()->pluck('id')->toArray();
        $serviceIds = Service::factory(60)->create()->pluck('id')->toArray();

        $usersServices = ServiceUser::factory(150)->make()->each(function ($userService) use ($userIds, $serviceIds, &$usedIds) {
            $userService->user_id = $userIds[array_rand($userIds)];
            $userService->service_id = $serviceIds[array_rand($serviceIds)];
            $userService->price = mt_rand(10, 400);
        })->toArray();

        foreach ($usersServices as $service) {
            try {
                ServiceUser::insert($service);
            } catch (\Exception $e) {
            }
        }
    }
}
